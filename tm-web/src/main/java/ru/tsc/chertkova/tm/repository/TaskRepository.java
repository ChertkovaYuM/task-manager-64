package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Проект 1"));
        add(new Task("Проект 2"));
        add(new Task("Проект 3"));
        add(new Task("Проект 4"));
    }

    public void create() {
        add(new Task(("Новая задача: " + System.currentTimeMillis())));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void save(@NotNull final Task task) {
        tasks.replace(task.getId(), task);
    }

}
