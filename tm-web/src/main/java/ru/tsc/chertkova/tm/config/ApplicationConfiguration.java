package ru.tsc.chertkova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.chertkova.tm")
public class ApplicationConfiguration {

}
