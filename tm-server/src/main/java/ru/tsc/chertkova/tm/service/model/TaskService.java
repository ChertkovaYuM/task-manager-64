package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.service.model.ITaskService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.model.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskService extends AbstractUserOwnerService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @Nullable List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public Task add(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUser().getId()).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(task.getName()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(task.getProjectId()).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUser().getId()).orElseThrow(TaskNotFoundException::new);
        taskRepository.saveAndFlush(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @Nullable Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.saveAndFlush(task);
        task = findById(userId, id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @Nullable Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.changeStatus(id, userId, status.getDisplayName());
        task = findById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return taskRepository.existsById(id);
    }

    @Override
    @Transactional
    public @Nullable Task findById(@Nullable final String userId,
                                   @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable Task task = taskRepository.findById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public Task removeById(@Nullable final String userId,
                           @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Task task = findById(userId, id);
        taskRepository.removeById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public Task remove(@Nullable final String userId,
                       @Nullable final Task task) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(findById(task.getUser().getId(), task.getId()))
                .orElseThrow(TaskNotFoundException::new);
        removeById(task.getUser().getId(), task.getId());
        return task;
    }

    @Override
    @Transactional
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        int size = taskRepository.getSize(userId);
        return size;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        taskRepository.clear(userId);
    }

    @Override
    @Nullable
    @Transactional
    public List<Task> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @Nullable List<Task> tasks = taskRepository.findAll(userId);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public List<Task> addAll(@NotNull final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        taskRepository.saveAll(tasks);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public List<Task> removeAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        taskRepository.deleteAll(tasks);
        return tasks;
    }

}
