package ru.tsc.chertkova.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.ITokenService;

@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
