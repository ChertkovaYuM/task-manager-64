package ru.tsc.chertkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractSystemListener;

@Component
public final class VersionListener extends AbstractSystemListener {

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Display program version.";

    public static final String ARGUMENT = "-v";

    @Override
    @EventListener(condition = "@versionListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

    @Override
    public String command() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public String argument() {
        return ARGUMENT;
    }

}
