package ru.tsc.chertkova.tm.listener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.chertkova.tm.listener.AbstractListener;

@Getter
@Component
@NoArgsConstructor
public abstract class AbstractDomainListener extends AbstractListener {

    @Autowired
    private IDomainEndpoint domainEndpoint;

}
