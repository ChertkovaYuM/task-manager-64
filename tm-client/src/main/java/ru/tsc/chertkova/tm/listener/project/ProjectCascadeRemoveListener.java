package ru.tsc.chertkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractProjectListener;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class ProjectCascadeRemoveListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-cascade-remove";

    @NotNull
    public static final String DESCRIPTION = "Project cascade remove.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCascadeRemoveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(getToken(), projectId));
    }

}
