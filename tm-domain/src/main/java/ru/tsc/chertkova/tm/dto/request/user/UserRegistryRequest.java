package ru.tsc.chertkova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;

@Setter
@Getter
@NoArgsConstructor
public class UserRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(@Nullable String login,
                               @Nullable String password,
                               @Nullable String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

}
