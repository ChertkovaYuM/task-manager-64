package ru.tsc.chertkova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String token;

    public AbstractUserRequest(@Nullable String token) {
        this.token = token;
    }

}
