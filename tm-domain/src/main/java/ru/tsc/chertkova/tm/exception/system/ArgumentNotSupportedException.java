package ru.tsc.chertkova.tm.exception.system;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String command) {
        super("Error! Argument ``" + command + "`` not supported...");
    }

}
