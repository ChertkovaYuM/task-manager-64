package ru.tsc.chertkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

public final class DataJsonFasterXmlLoadRequest extends AbstractUserRequest {

    public DataJsonFasterXmlLoadRequest(@Nullable String token) {
        super(token);
    }

}
